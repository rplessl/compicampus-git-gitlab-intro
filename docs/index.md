# Welcome to the Course

Welcome to the CompiCampus "Dive in to Git and GitLab" Course from 2022-10-17!

I set up the course like this:

First there will be an input about Git, GitLab including some details about the history and design goals of Git. Then you can take the inputs, learn the most important Git commands and follow the exercises for Git and GitLab from the slide deck. With the goal to learn how to use both Web and command line tools.

The second part of the course will be in the form of an accompanied exercise.

Have fun discovering Git and GitLab with it's overwhelming features :smile:!

Cheers, Roman

And ask me if you have any questions :raised_hand:

## Slides and Exercises

The slide deck has informational and exercises parts, the exercises are marked with red slides.

[Slides and Exercises (Download)](https://github.com/rplessl/compicampus-git-int
ro/raw/master/docs/CompiCampus%20-%20Dive%20in%20to%20Git%20and%20GitHub%20-%202
022-10-12.pdf)

[Slides and Exercises (Viewer in GitHub)](https://github.com/rplessl/compicampus
-git-intro/blob/master/docs/CompiCampus%20-%20Dive%20in%20to%20Git%20and%20GitHu
b%20-%202022-10-12.pdf)

- The slide deck has informational parts marked with blue slides.
- The included exercises are marked with red slides.

## Agenda and Goals of the Course Today - 2022-10-17

1. Git - Introduction to Version Control Systems and Git

2. Introduction to GitLab (I): Overview,  Documentation - Create or Login to your GitLab Account, Create a new Project, Using a Repository, General Settings (public vs. private)

3. Learning Path and Exercises based on your knowledge and experience with Git / GitLab including a bunch of practical exercises:

   1. Basic Git Workflow, Basic Git Commands
   2. Using GitLab (II), including: Login or Create a GitLab Account, Create a new Project, Using Repositories and How to use local Git with GitLab
   3. Working with Git branches
   4. Interact with each other’s and other coders: Git Branching, Code Changes and Commits, Pull and Merge Requests, Comments

4. How to use GitLab (III):  Intro to CI/CD {gitlab-ci}, Issue Tracking, Wiki, GitLab Pages, Settings

## About me

* [github.com/rplessl](https://github.com/rplessl])
* [nuvibit.com](https://nuvibit.com)
* [plessl-burkhardt.ch](https://plessl-burkhardt.ch)

### Public Link to Slide Source

[Dive in Git and GitHub Slides in Google Presentation](https://docs.google.com/presentation/d/1p11ehYNbOYlN-q_oFjBC86GVhdMCby6TbJXvXWrS-6s/edit?usp=sharing)


---

## License and Copyright

My Material is licensed with the "Attribution 4.0 International (CC BY 4.0)" license.

You are free to:

- Share — copy and redistribute the material in any medium or format
- Adapt — remix, transform, and build upon the material for any purpose, even commercially.

Under the following terms:

- Attribution — You must give appropriate credit, provide a link to the license, and indicate if changes were made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.
- No additional restrictions — You may not apply legal terms or technological measures that legally restrict others from doing anything the license permits.

Parts of the content in the slide are taken from [GitLab Documentation](https://docs.gitlab.com) and [Git Book](https://git-scm.com/book/en/v2), which are using the same CC BY 4.0 license or the Version 3.

---

Created with :heart: by Roman Plessl, plessl-burkhardt.ch and last updated on 2022-10-17

